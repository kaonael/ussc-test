from django.test import TestCase
from .utils import validate_name, validate_node
from .models import Classificator
from rest_framework.test import APIRequestFactory
from rest_framework.utils import json
from django.urls import reverse
from rest_framework import status
from .views import TreeViewSet

factory = APIRequestFactory()
tree_view_set = TreeViewSet


class TreeTestCase(TestCase):
    fixtures = ['classificator_initial.json']

    def test_node_validation(self):
        node = Classificator.objects.get(pk=25)
        self.assertFalse(validate_node(node, 'Компилируемые языки'))
        self.assertTrue(validate_node(node, 'RarePlusPlus'))
        self.assertFalse(validate_node(node, 'CPlusPlus'))
        node = Classificator.objects.get(pk=26)
        self.assertFalse(validate_node(node, 'Интерпретируемые языки'))
        self.assertTrue(validate_node(node, 'Python Super Snake'))
        self.assertFalse(validate_node(node, 'IronPython'))

    def test_name_validation(self):
        """
        Проверка регулярного выражения на разных строках
        """
        self.assertEqual(True, validate_name("Строка для проверки из русских символов с пробелами"))
        self.assertEqual(False, validate_name(' строка с пробела'))
        self.assertEqual(False, validate_name(''))
        self.assertEqual(False, validate_name(' '))
        self.assertEqual(False, validate_name('\b'))
        self.assertEqual(False, validate_name('	0x0B'))
        self.assertEqual(False, validate_name('\t'))
        self.assertEqual(False, validate_name('Строка с двойным  пробелом'))
        self.assertEqual(True, validate_name('Русско-latinskie bukвы'))
        self.assertEqual(True, validate_name('Русско-татарский'))
        self.assertEqual(False, validate_name(' Пробел '))
        self.assertEqual(False, validate_name(' Test '))

    def test_create_valid_node(self):
        view = tree_view_set.as_view(actions={'post': 'create'})
        request = factory.post(reverse('easy_tree-list'),
                               json.dumps({'name': 'CPython вторая версия',
                                           'parent': '28'
                                           }),
                               content_type='application/json')
        response = view(request)
        child = Classificator.objects.get(pk=response.data.get('id'))
        children = Classificator.objects.get(pk=28).get_children()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(child in children)

    def test_create_invalid_node(self):
        view = tree_view_set.as_view(actions={'post': 'create'})
        request = factory.post(reverse('easy_tree-list'),
                               json.dumps({'name': 'Интерпретируемые языки',
                                           'parent': '41'
                                           }),
                               content_type='application/json')
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_node(self):
        view = tree_view_set.as_view(actions={'delete': 'destroy'})
        node = Classificator.objects.get(pk=25)
        request = factory.delete(reverse('easy_tree-detail', args=(node.pk,)))
        response = view(request, pk=node.pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Classificator.objects.filter(parent=node))

    def test_update_invalid_node(self):
        view = tree_view_set.as_view(actions={'put': 'update'})
        node = Classificator.objects.get(pk=26)
        request = factory.put(reverse('easy_tree-detail', args=(node.pk,)),
                              json.dumps({'name': 'CPlusPlus',
                                          'parent': f'{node.parent_id}'
                                          }),
                              content_type='application/json')
        response = view(request, pk=node.pk)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_valid_node(self):
        view = tree_view_set.as_view(actions={'put': 'update'})
        node = Classificator.objects.get(pk=28)
        request = factory.put(reverse('easy_tree-detail', args=(node.pk,)),
                              json.dumps({'name': 'Empty',
                                          'parent': f'{node.parent_id}'
                                          }),
                              content_type='application/json')
        response = view(request, pk=node.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


