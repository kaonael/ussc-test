from rest_framework import viewsets, filters, mixins
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import TreeSerializer
from .models import Classificator


class TreeViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin, mixins.RetrieveModelMixin):
    """
    Класс для работы создания CRUD для модели Classificator
    """
    queryset = Classificator.objects.all()
    serializer_class = TreeSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['name']
