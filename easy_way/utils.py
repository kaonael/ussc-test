from .models import Classificator
import re


def validate_name(string: str) -> bool:
    """
    Проверяет соответствие имени по условиям задачи.
    :param string: Строка на проверку.
    :return: True если строка соответствует шаблону.
    """
    pattern = re.compile('^([-a-zA-ZА-Яа-я]+\s)*[-a-zA-ZА-Яа-я]+$')
    return True if pattern.match(string) else False


def validate_node(parent: Classificator, name: str) -> bool:
    """
    Проверяет условие уникальности имени в пределах ветки.
    :param parent: Предполагаемый родитель инстанса.
    :param name: Имя на проверку уникальности.
    :return: True если в ветке не найдено аналогичного имени.
    """
    if parent:
        if len(parent.get_family().filter(name=name)) == 0:
            return True
        else:
            return False
    else:
        if len(Classificator.objects.filter(name=name, level=0)) == 0:
            return True
        else:
            return False

