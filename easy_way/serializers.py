from rest_framework import serializers
from .models import Classificator
from .utils import validate_node, validate_name


class TreeSerializer(serializers.ModelSerializer):
    """
    Сериализатор для Classificator
    """

    def validate(self, attrs):
        """
        Дополнительная валидация по условию задачи.
        :param attrs: Данные для валидации.
        :return: Возвращает провалидированные данные.
        """
        if validate_name(attrs['name']) is False:
            raise serializers.ValidationError("Имя не соотвествует шаблону")
        if validate_node(attrs['parent'], attrs['name']) is False:
            raise serializers.ValidationError("Имя не уникально в рамках ветки")
        return attrs

    class Meta:
        model = Classificator
        fields = '__all__'
