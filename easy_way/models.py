from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Classificator(MPTTModel):
    """
    Реализация NestedSet на основе библиотеки Django-MTTP
    """

    name = models.CharField(max_length=50, blank=False, null=False)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return f'{self.name} on level {self.level}'

    class Meta:
        db_table = 'classificators'

    class MPTTMeta:
        order_insertion_by = ['name']

