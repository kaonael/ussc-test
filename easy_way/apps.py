from django.apps import AppConfig


class EasyWayConfig(AppConfig):
    name = 'easy_way'
