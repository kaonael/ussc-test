from rest_framework import serializers
from .models import Classificator
from easy_way.utils import validate_name
from hard_way.utils import validate_name_in_tree, validate_position


class TreeSerializer(serializers.ModelSerializer):
    """
    Сериализатор для Classificator
    """

    def validate(self, attrs):
        """
        Дополнительная валидация по условию задачи.
        :param attrs: Данные для валидации.
        :return: Возвращает провалидированные данные.
        """
        print(attrs.__dict__)
        if validate_name(attrs['name']) is False:
            raise serializers.ValidationError("Имя не соотвествует шаблону")
        if validate_name_in_tree(attrs['parent'], attrs['name']) is False:
            raise serializers.ValidationError("Имя не уникально в рамках ветки")
        try:
            instance = Classificator.objects.get(pk=attrs['id'])
            if validate_position(source=instance, target=attrs['parent']) is False:
                raise serializers.ValidationError("Нельзя перемещать родителя под потомка")
        except KeyError:
            pass
        return attrs

    class Meta:
        model = Classificator
        fields = '__all__'
