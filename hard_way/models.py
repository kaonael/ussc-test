from django.db import models


class Classificator(models.Model):
    """
    Модель для теста рекурсивных запросов.
    """
    parent = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True,
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=50)

    def __str__(self):
        name = self.parent.name if self.parent is not None else "None"
        return f'{self.name}/{name}'

    def get_descendants(self, include_self=False):
        """
        Получаем потомков с помощью рекурсивного запроса
        :param include_self: True для включения инстанса в выборку
        :return: RawQuerySet
        """
        sql = """WITH RECURSIVE t AS (
          SELECT * FROM hard_classificators WHERE id = %s
          UNION
          SELECT hard_classificators.* FROM hard_classificators JOIN t ON hard_classificators.parent_id = t.id
        )
        SELECT * FROM t"""

        if not include_self:
            sql += " OFFSET 1"

        return Classificator.objects.raw(sql, [self.id])

    def move_to(self, target):
        self.parent = target
        self.save()

    @property
    def get_branch(self):
        """
        Находит ветку дерева.
        :return: List из элементов дерева.
        """
        parent = self.parent
        data = [self]
        if self.parent is None:
            return data
        else:
            data.append(self.parent)
            while True:
                if parent.parent is None:
                    break
                parent = parent.parent
                data.append(parent)
        return data

    class Meta:
        db_table = 'hard_classificators'
