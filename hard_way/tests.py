from django.test import TestCase
from .utils import validate_name_in_tree, validate_position
from .models import Classificator


class UtilsTest(TestCase):
    fixtures = ['classificator_initial.json']

    def test_validate_name(self):
        instance = Classificator.objects.get(pk=44)
        self.assertTrue(validate_name_in_tree(instance,''))


# Create your tests here.
