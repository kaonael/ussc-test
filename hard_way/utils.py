from .models import Classificator


def validate_name_in_tree(parent: Classificator, name: str) -> bool:
    """
    Проверяет условие уникальности имени в пределах ветки. Для реализации с AM
    :param parent: Предполагаемый родитель инстанса.
    :param name: Имя на проверку уникальности.
    :return: True если в ветке не найдено аналогичного имени.
    """
    if parent is None:
        return True
    branch = parent.get_branch
    branch += parent.get_descendants()
    buf = [i for i in branch if i.name == name]
    if buf:
        return False
    else:
        return True


def validate_position(target: Classificator, source: Classificator):
    """
    Проверка перемещения узла
    :param target: Куда перемещаем
    :param source: Что перемещаем
    :return: True в случае  успеха
    """
    if target.parent is None:
        return True
    children = source.get_descendants(include_self=True)
    list = [i for i in children if i == target]
    if target in list:
        return False
    else:
        return True



