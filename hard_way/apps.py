from django.apps import AppConfig


class HardWayConfig(AppConfig):
    name = 'hard_way'
